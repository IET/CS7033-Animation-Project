import re
import os
import fileinput

search_dir = os.getcwd()

#File directory
#os.path.dirname(os.path.realpath(__file__))

for root, _, files in os.walk(search_dir):
	for f in files:
		output = ""
		fullpath = os.path.join(root, f)
		if re.match(".*\\.(dae)", f, flags=re.IGNORECASE):
			print ("Found dae file: {0}".format(f))
			with open(fullpath) as file:
				for line in file:
					line = re.sub(r'C:.*[\\/](.*\.(jpg|tga|png|bmp))', r'\1', line)
					output += line
				file.close()

			with open(fullpath, 'w') as file:
				file.write(output)
				file.close