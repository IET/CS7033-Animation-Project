#pragma once
// GLEW
#define GLEW_STATIC
#include <GL/glew.h>
#include <IL\il.h>
#include <iostream>

class TextureUtils {
public:
	static GLuint LoadTextureFromFile(GLchar const * path);
};