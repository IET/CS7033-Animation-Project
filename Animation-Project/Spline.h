#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

#include <vector>

class Spline
{
	struct SplinePoint
	{
		glm::vec3 position;
		float time;	

		SplinePoint()
		{
			position = glm::vec3(0);
			time = 0;
		}

		SplinePoint(glm::vec3 p, float t)
			:position(p), time(t)
		{}
	};
public:
	Spline(void);

	bool isLooped;

	std::vector<SplinePoint> points;

	glm::vec3 GetPositionLinear(float time);
	glm::vec3 GetPositionCosine(float time);
	glm::vec3 GetPositionCubic(float time);

	void AddPoint(glm::vec3 position, float time);

	~Spline(void);
private:
	int GetPointValues(SplinePoint &start, SplinePoint &end, float &t);

	glm::vec3 LinearInterpolate(glm::vec3 p0, glm::vec3 p1, float mu);
	glm::vec3 CosineInterpolate(glm::vec3 p0, glm::vec3 p1, float mu);
	glm::vec3 CubicInterpolate(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, float mu);
};

