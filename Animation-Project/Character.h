#pragma once
#include "AnimationStateMachine.h"
#include "Model.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>

class Character
{
public:
	Character();
	//Character(Model m);
	Character (std::string modelFilePath);

	void LoadModel(std::string modelFilePath);

	Model model;

	AnimationStateMachine stateMachine;
	virtual void Update(float deltaTime) = 0;

	void Draw(glm::mat4 v,glm::mat4  p, float deltaTime);

	bool LoadAnimations();

	~Character()
	{
//		if (m != nullptr)
//			delete m;
	}
};