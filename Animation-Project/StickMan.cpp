#include "StickMan.h"


StickMan::StickMan(void)
	:groundPlane(-25.0f)
{
	walkSpeed = 50.0f;
	runSpeed = 130.0f;
	facing = glm::vec3(0,0,1);
	turnAngle = 0;
}


void StickMan::Jump()
{
	if (this->model.animationStateMachine.currentState->name != "jump")
		this->model.animationStateMachine.StateBlip("jump");
}

void StickMan::Punch()
{
	if (this->model.animationStateMachine.currentState->name != "punch")
		this->model.animationStateMachine.StateBlip("punch");
}

void StickMan::Walk(float deltaTime)
{
	if (this->model.animationStateMachine.currentState->name != "walk" && this->model.animationStateMachine.blipTime == -1)
		this->model.animationStateMachine.ChangeState("walk");
	if (this->model.animationStateMachine.currentState->name != "punch")
		this->model.moveBy( glm::normalize(glm::vec3(model.modelMatrix[2])) * walkSpeed * deltaTime);
}

void StickMan::Run(float deltaTime)
{
	if (this->model.animationStateMachine.currentState->name != "run" && this->model.animationStateMachine.blipTime == -1)
		this->model.animationStateMachine.ChangeState("run");
	if (this->model.animationStateMachine.currentState->name != "punch")
		this->model.moveBy(glm::normalize(glm::vec3(model.modelMatrix[2])) * runSpeed * deltaTime);
}

void StickMan::Idle()
{
	if (this->model.animationStateMachine.currentState->name != "idle" && this->model.animationStateMachine.blipTime == -1)
		this->model.animationStateMachine.ChangeState("idle");
}

void StickMan::Turn(float angle)
{
	this->model.RotateBy(glm::vec3(0,angle,0));
}

StickMan::~StickMan(void)
{

}
