#include "AnimationStateMachine.h"
#include <iostream>

AnimationStateMachine::AnimationStateMachine(void)
{
	isPlaying = true;
	previousState = nullptr;
	nextState = nullptr;
	transitionEnd = -1;
	blipTime = -1; 
}

void AnimationStateMachine::Update(float deltaTime)
{

	if (!isPlaying)
		return;

	currentTime += deltaTime;


	if (transitionEnd != -1 && currentTime >= transitionEnd)
	{
		currentState = nextState;
		nextState = nullptr;
		transitionEnd = -1;

		if (currentTime > currentState->clip->mDuration)
			currentTime = fmod(currentTime, (float)currentState->clip->mDuration);

		if (!currentState->isLooped)
			currentTime = 0;
	}
	
	if (blipTime != -1 && currentTime >= blipTime && transitionEnd == -1)
	{
		ChangeState(previousState->name);
		blipTime = -1;
	}

	if (currentTime > currentState->clip->mDuration && transitionEnd == -1)
	{
		if (currentState->isLooped)
			currentTime = 0;
	}


}

void AnimationStateMachine::Play()
{
	isPlaying = true;
}

void AnimationStateMachine::Pause()
{
	isPlaying = false;
}

void AnimationStateMachine::AddState(std::string name, aiAnimation* clip)
{
	AnimationState state(name, clip);
	allStates[name] = state;
	if (allStates.size() == 1)
		currentState = &allStates[name];
}

void AnimationStateMachine::AddTransition(std::string source, std::string destination, float time)
{
	if (allStates.find(source) == allStates.end())
	{
		std::cout << "State " << source << " not found. Failed to load transition: " << source << " > " << destination;
	}
	if (allStates.find(destination) == allStates.end())
	{
		std::cout << "State " << destination << " not found. Failed to load transition: " << source << " > " << destination;
	}
	
	allStates[source].stateTransitions[destination] = AnimationState::Transition(&allStates[destination], time);
}

bool AnimationStateMachine::ChangeState(std::string targetState)
{
	//Don't change state while transitioning
	if (transitionEnd != -1)
		return false;

	if (targetState == currentState->name || (nextState != nullptr && nextState->name == targetState))
	{
		//nothing to be done here, either there alredy or currently transitioning
		return true;
	}

	if (currentState == nullptr)
	{
		std::cout << "ERROR::State Machine in null state." << std::endl;
		return false;
	}

	if (currentState->stateTransitions.find(targetState) == currentState->stateTransitions.end())
	{
		std::cout << "ERROR::No Transition Found From " << currentState->name << " to " << targetState << std::endl;
		return false;
	}

	AnimationState::Transition targetTransition = currentState->stateTransitions[targetState];

	transitionStart = currentTime;
	transitionEnd = currentTime + targetTransition.duration;
	nextState = targetTransition.targetState;
	return true;
}

bool AnimationStateMachine::StateBlip(std::string targetState)
{
	previousState = currentState;
	if (!ChangeState(targetState))
		return false;
	blipTime = nextState->clip->mDuration;//transitionEnd + nextState->clip->mDuration - (currentState->stateTransitions[previousState->name].duration/2);
	return true;
}

AnimationStateMachine::~AnimationStateMachine(void)
{
}
