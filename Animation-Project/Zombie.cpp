#include "Zombie.h"
#include "DebugOutput.h"

static std::vector<Zombie> allZombies();

Zombie::Zombie(void)
{
	inverseSpeed = 0.5f + ((float) (rand() % 3)) + ((float)(rand() % 100)/100.0f);
	stepSize = 2.5f;
	turnSpeed = 1.2f;
	deathTime = -1;
	doDraw = true;
	this->model.animationStateMachine.currentTime = ((float) (rand() % 3)) + ((float)(rand() % 100)/100.0f);
}

void Zombie::Draw(glm::mat4 v,glm::mat4  p, float deltaTime)
{
	if (doDraw)
		model.Draw(v, p, deltaTime);
	if (this->model.animationStateMachine.currentState->name == "death" && this->model.animationStateMachine.currentTime >= (this->model.animationStateMachine.currentState->clip->mDuration * 0.95f))
		doDraw = false;
}

void Zombie::Update (float deltaTime)
{
	//model.Update(deltaTime);
	if (!doDraw)
		return;

	if (this->model.animationStateMachine.currentTime >= deathTime && deathTime != -1 && this->model.animationStateMachine.currentState->name != "death")
	{
		glm::vec3 currDir = glm::normalize(glm::vec3(model.modelMatrix[2]));
		glm::vec3 targetDir = glm::normalize(*target - model.getLocation());
		float dot = glm::dot(currDir, targetDir);
		if (dot > 1) dot = 1;
		if (dot < 0) dot = 0;
		float angle = acos(dot);
		model.RotateBy(glm::vec3(0, angle, 0));

		this->model.animationStateMachine.ChangeState("death");
	}

	if (this->model.animationStateMachine.currentTime > inverseSpeed && deathTime == -1)
	{
		this->model.animationStateMachine.currentTime = 0;
	}
	else if (this->model.animationStateMachine.currentTime < model.animationStateMachine.currentState->clip->mDuration && deathTime == -1)
	{
		if (target != nullptr)
		{
			DebugOutput::DrawLine(model.location, model.location +  (glm::normalize(glm::vec3(model.modelMatrix[2])) * 10.0f), glm::mat4(1), glm::vec3(0, 1, 0));

			DebugOutput::DrawLine(model.location, *target, glm::mat4(1), glm::vec3(0, 0, 1));
			
			glm::vec3 currDir = glm::normalize(glm::vec3(model.modelMatrix[2].x, 0, model.modelMatrix[2].z));
			glm::vec3 targetDir = glm::normalize(glm::vec3(target->x, 0, target->z) - glm::vec3(model.location.x, 0, model.location.z));

			glm::vec3 axis = glm::normalize(glm::cross(currDir, targetDir));

			model.RotateBy(deltaTime * turnSpeed, axis);

			if (glm::length(*target - model.getLocation()) > 10.0f)
				model.moveBy(glm::normalize(glm::vec3(model.modelMatrix[2]) * deltaTime * stepSize));
		}
	}
}

void Zombie::Kill(float time)
{
	deathTime = this->model.animationStateMachine.currentTime + (time * 0.6f);
}

Zombie::~Zombie(void)
{
}
