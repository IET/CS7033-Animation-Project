#pragma once
#include <assimp/scene.h>
#include <vector>
#include <unordered_map>
#include <string>

class AnimationState 
{
public:
	struct Transition {
		AnimationState* targetState;
		float duration;	

		Transition():targetState(nullptr), duration(0){};

		Transition(AnimationState *targetState, float t)
		{
			this->targetState = targetState;
			duration = t;
		}
	};

	AnimationState(void);
	AnimationState(std::string name, aiAnimation* clip);

	aiAnimation* clip;

	bool isLooped;
	std::string name;
	std::unordered_map<std::string, Transition> stateTransitions; 

	~AnimationState(void);


};

