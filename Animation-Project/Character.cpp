#include "Character.h"

Character::Character()
{
}

Character::Character (std::string modelFilePath)
{
	model.load(modelFilePath);
}

void Character::LoadModel(std::string modelFilePath)
{
	this->model.load(modelFilePath);
}

void Character::Draw(glm::mat4 v,glm::mat4  p, float deltaTime)
{
	model.Draw(v, p, deltaTime);
}