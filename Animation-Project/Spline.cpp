#include "Spline.h"
#include <math.h>

Spline::Spline(void)
{
	isLooped = false;
}

glm::vec3 Spline::GetPositionLinear(float time)
{
	SplinePoint startPoint, endPoint;
	GetPointValues(startPoint, endPoint, time);

	float fraction = (time - startPoint.time)/(endPoint.time - startPoint.time);
	
	return LinearInterpolate(startPoint.position, endPoint.position, fraction);
}

glm::vec3 Spline::GetPositionCosine(float time)
{
	SplinePoint startPoint, endPoint;
	GetPointValues(startPoint, endPoint, time);

	float fraction = (time - startPoint.time)/(endPoint.time - startPoint.time);

	return CosineInterpolate(startPoint.position, endPoint.position, fraction);
}

glm::vec3 Spline::GetPositionCubic(float time)
{
	SplinePoint startPoint, endPoint;

	int endPointIndex = GetPointValues(startPoint, endPoint, time);

	glm::vec3 p0, p1, p2, p3;

	p1 = startPoint.position; 
	p2 = endPoint.position;
	
	if (endPointIndex == 1 || endPointIndex == (points.size() - 1))
	{
		//Calculate additional points p0 & p3 as necessary
		if (endPointIndex != 1)
		{
			p0 = points[endPointIndex - 2].position;
		}
		else
		{
			p0 = p1 + (p1 - p2);
		}

		if (endPointIndex != points.size() -1 )
		{
			p3 = points[endPointIndex + 1].position;		
		}
		else
		{
			p3 = p2 + (p2 - p1);
		}

	}
	else
	{
		p3 = points[endPointIndex + 1].position;		
		p0 = points[endPointIndex - 2].position;
	}
	
	float fraction = (time - startPoint.time)/(endPoint.time - startPoint.time);

	return CubicInterpolate(p0, p1, p2, p3, fraction);;
}

glm::vec3 Spline::LinearInterpolate(glm::vec3 p0, glm::vec3 p1, float mu)
{
	return (glm::vec3((p0 * (1 - mu)) + (p1 * mu)));
}

glm::vec3 Spline::CosineInterpolate(glm::vec3 p0, glm::vec3 p1, float mu)
{
	mu = (1 - cos(mu * 3.14))/2.0f;
	return (glm::vec3((p0 * (1 - mu)) + (p1 * mu)));
}

glm::vec3 Spline::CubicInterpolate(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, float mu)
{
	float mu2 = mu * mu;
	glm::vec3 a0, a1, a2, a3;

	//Standard Cubic
	/*
	a0 = p3 - p2 - p0 + p1;
	a1 = p0 - p1 - a0;
	a2 = p2 - p1;
	a3 = p1;
	*/

	//Catmull-Rom
	a0 = (-0.5f * p0) + (1.5f * p1) - (1.5f * p2) + (0.5f * p3);
	a1 = p0 - (2.5f * p1) + (2.0f * p2) - (0.5f * p3);
	a2 = (-0.5f * p0) + (0.5f * p2);
	a3 = p1;

	return (glm::vec3((a0 * mu * mu2) + (a1 * mu2) + (a2 * mu) + (a3)));
}

void Spline::AddPoint(glm::vec3 position, float time)
{
	points.push_back(SplinePoint(position, time));
}

int Spline::GetPointValues(SplinePoint &start, SplinePoint &end, float &t)
{
	if (! isLooped)
	{
		if (t >= points[points.size() -1].time)
			t = points[points.size() -1].time;
	}
	else
	{
		if (t >= points[points.size() -1].time)
		{
			t = t / (points[points.size() -1].time - points[0].time);
			t -= floor(t);
			t *= (points[points.size() -1].time - points[0].time);
		}
	}

	if (t <= points[0].time)
	{
		t = points[0].time;
		start = points[0];
		end = points[1];
		return 1;
	}

	int i =0;
	SplinePoint sp = points[i];
	while (t > sp.time)
	{
		sp = points[++i];
	}

	end = sp;
	start = points[i-1];
	return i;
}

Spline::~Spline(void)
{
}
