#pragma once
#include "character.h"
class StickMan :
	public Character
{
public:
	StickMan(void);
	float walkSpeed;
	float runSpeed;

	//glm::vec3 velocity;
	glm::vec3 facing;
	float turnAngle;

	const float groundPlane;

	void Update(float deltaTime)
	{
		//model.Update(deltaTime);
	}

	void Jump();
	void Punch();
	void Run(float deltaTime);
	void Walk(float deltaTime);
	void Idle();

	void Turn(float angle);

	~StickMan(void);
};

