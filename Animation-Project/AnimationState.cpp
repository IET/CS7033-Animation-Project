#include "AnimationState.h"


AnimationState::AnimationState(void)
{
	isLooped = true;
}

AnimationState::AnimationState(std::string name, aiAnimation* clip)
{
	this->name = name;
	this->clip = clip;
	isLooped = true;
}


AnimationState::~AnimationState(void)
{
}
