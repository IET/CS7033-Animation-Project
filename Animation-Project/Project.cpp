// OGLProject.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <glfw3.h>

//GL Maths
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Include AntTweakBar
//#include <AntTweakBar.h>

//Local Classes
#include "model.h"
#include "ShaderManager.hpp"
#include "Camera.h"
#include "TextureUtils.h"
#include "CubeMap.hpp"

#include "Character.h"
#include "StickMan.h"
#include "Zombie.h"

#include "Lighting.h"
#include "Spline.h"

#include <sstream> // for ostringstream

#include "DebugOutput.h"

#include <glm\quaternion.hpp>

static const int MAX_BONES = 100;

//Target Framerate (will not exceed)
const int TARGET_FPS = 60;

//Handles Program Initialisation 
bool glInit();

//Camera and Input Variables
Camera camera(glm::vec3(0.0f, 7.0f, 100.0f));
bool paused = false;
bool debug = false;

bool keys[1024];
GLfloat lastX = 400, lastY = 300;
bool firstMouse = true;


// Function prototypes
//GLFW Input Callbacks
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);

void handleInput();
void drawScene(glm::mat4 v, glm::mat4 p);

//Window For OpenGL App
GLFWwindow* window;
// Window dimensions
const GLuint WIDTH = 1200, HEIGHT = 900;
//Clipping Planes
float near = 0.1f;
float far = 10000.0f;

//For time-variant operations
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;
GLfloat t = 0.0;
GLint nbFrames = 0;

Model bob;
Model hand;
Model peter;
Model sphere;
Model spaceStation;
Model turtle;
StickMan stickMan;
Zombie zombie, zombie1, zombie2, zombie3, zombie4;

GLuint sphereTex;
GLuint stickManTexture;

//Point to seek for inverse kinematics
glm::vec3 ikTarget = glm::vec3(6.15f, -9.79f, 0.38f);

Shader *shader, *skinningShader, *texShader;
CubeMap cubeMap;

float splineTime = 0.0f;
float splineTargetTime = 15.0f;

glm::vec3 splineStart = glm::vec3(5.0f, 0.0f, 0.0f);;
glm::vec3 splineEnd = glm::vec3(50.0f, 20.0f, 0.0f);
float splineDirection = 1.0f;

Spline testSpline;

glm::quat cameraOrientation = glm::angleAxis(0.0f, glm::vec3(0, 1, 0));

int _tmain(int argc, _TCHAR* argv[])
{
		//Initialise OpenGL
	if (!glInit()) {
		std::cout << "Failed to initialise OpenGL";
		std::cin;
		return -1;
	}

	testSpline.AddPoint(glm::vec3(0), 0);	
	testSpline.AddPoint(glm::vec3(10.0f, 0.0f, 0.0f), 1.0f);
	testSpline.AddPoint(glm::vec3(10.0f, 10.0f, 0.0f), 5.0f);
	testSpline.AddPoint(glm::vec3(0.0f, 10.0f, 0.0f), 12.0f);
	testSpline.AddPoint(glm::vec3(50.0f, 0.0f, 0.0f), 25.0f);
	testSpline.AddPoint(glm::vec3(0.0f, 0.0f, 0.0f), 35.0f);

	testSpline.isLooped = true;
	glm::mat4 view;
	camera.Position = stickMan.model.getLocation() + (550.0f * glm::vec3(stickMan.model.modelMatrix[1])) + (-600.0f * glm::vec3(stickMan.model.modelMatrix[2]));
	camera.Front = glm::normalize(stickMan.model.getLocation() - camera.getPosition());

	// Game loop
	while (!glfwWindowShouldClose(window))
	{

		// Set frame time
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		//Should lock framerate at target framerate
		if(deltaTime < (1.0f/TARGET_FPS))
			continue;
		lastFrame = currentFrame;
	
		nbFrames++;
		//Output frame time to console
		if ( currentFrame - lastFrame >= 1.0 ){ // If last prinf() was more than 1 sec ago

			// printf and reset timer

			printf("%f ms/frame\n", 1000.0/double(nbFrames));

			nbFrames = 0;

			lastFrame += 1.0;
		}

		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();
		handleInput();

		//Set BG Color & Clear the colorbuffer
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		
		splineTime += (splineDirection) * deltaTime;
		if (splineTime < 0 || splineTime > splineTargetTime)
			splineDirection *= -1;

		camera.Position = (400.0f * glm::vec3(stickMan.model.modelMatrix[1])) + (-600.0f * glm::vec3(stickMan.model.modelMatrix[2]));
		
		camera.Position = stickMan.model.getLocation() + glm::vec3(glm::mat4_cast(cameraOrientation) *  glm::vec4(camera.Position, 1));

		camera.Front = glm::normalize(stickMan.model.getLocation() - camera.getPosition());
		camera.Right = glm::cross(camera.Front, glm::vec3(0,1,0));
		camera.Up = glm::cross(camera.Right, camera.Front);
		
		view = glm::lookAt(camera.Position, stickMan.model.getLocation(), camera.Up);
			

		// Create camera transformation
		//view = camera.GetViewMatrix();
		
		//Create Projection Matrix
		glm::mat4 projection;	
		projection = glm::perspective(camera.Zoom, (float)WIDTH/(float)HEIGHT, near, far);

		splineTime += deltaTime;
		ikTarget = testSpline.GetPositionCosine(splineTime);

		//Draw scene
		drawScene(view, projection);

		DebugOutput::Update(view, projection);

		// Swap the screen buffers
		glfwSwapBuffers(window);
		t += deltaTime;
	}
		// Terminate GLFW, clearing any resources allocated by GLFW.
	glfwTerminate();
	return 0;
}


void drawScene(glm::mat4 v, glm::mat4 p)
{
	//Draw CubeMap
	cubeMap.getShader()->enableShader();
	cubeMap.getShader()->setUniformMatrix4fv("projectionMat", p);
	cubeMap.getShader()->setUniformMatrix4fv("viewMat", glm::mat4(glm::mat3(v)));
	cubeMap.drawSkyBox();
	cubeMap.getShader()->disableShader();

/*
	time /= 2.0f;
	hand.getShader()->enableShader();
	hand.getShader()->setUniformVector3fv("camPos", camera.getPosition());
	Lighting::SetupLights(skinningShader);
	hand.Draw(v, p, time);
	hand.getShader()->disableShader();
*/

	spaceStation.getShader()->enableShader();
	spaceStation.getShader()->setUniformVector3fv("camPos", camera.getPosition());
	Lighting::SetupLights(spaceStation.getShader());
	spaceStation.Draw(v, p);
	spaceStation.getShader()->disableShader();

/*
	bob.getShader()->enableShader();
	Lighting::SetupLights(skinningShader);
	bob.getShader()->setUniformVector3fv("camPos", camera.getPosition());

	bob.UpdateAnimation(time);
	bob.DrawIK(v, p, "spine", 4, sphere.getLocation());
	bob.getShader()->disableShader();
*/

	stickMan.model.getShader()->enableShader();
	Lighting::SetupLights(skinningShader);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, stickManTexture);
	stickMan.model.getShader()->setUniform1i("texture_diffuse1", 1);

	stickMan.model.Draw(v, p, deltaTime);

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	stickMan.model.getShader()->disableShader();


/*
	turtle.getShader()->enableShader();
	Lighting::SetupLights(skinningShader);
	turtle.Draw(v, p, deltaTime);
	turtle.getShader()->disableShader();
*/


	zombie.model.getShader()->enableShader();
	zombie.Update(deltaTime);
	zombie.Draw(v, p, deltaTime);
	zombie.model.getShader()->disableShader();

	zombie1.model.getShader()->enableShader();
	zombie1.Update(deltaTime);
	zombie1.Draw(v, p, deltaTime);
	zombie1.model.getShader()->disableShader();

	zombie2.model.getShader()->enableShader();
	zombie2.Update(deltaTime);
	zombie2.Draw(v, p, deltaTime);
	zombie2.model.getShader()->disableShader();

	zombie3.model.getShader()->enableShader();
	zombie3.Update(deltaTime);
	zombie3.Draw(v, p, deltaTime);
	zombie3.model.getShader()->disableShader();

	zombie4.model.getShader()->enableShader();
	zombie4.Update(deltaTime);
	zombie4.Draw(v, p, deltaTime);
	zombie4.model.getShader()->disableShader();

	sphere.setLocation(ikTarget);
	//sphere.CosineInterpolatePosition(splineStart, splineEnd, splineTime/splineTargetTime);
	sphere.getShader()->enableShader();
	//Lighting::SetupLights(texShader);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, sphereTex);

	//sphere.getShader()->setUniform1i("texture_diffuse1", 1);
	//sphere.Draw(v, p);

	glBindTexture(GL_TEXTURE_2D, 0);
	glActiveTexture(GL_TEXTURE0);
	sphere.getShader()->disableShader();
	
	if (debug)
	{
		bob.DrawJoints();
		bob.DrawBones(v, p);
	}
		
#ifdef _DEBUG
	DebugOutput::DrawAll(v, p);
#endif

}


bool glInit() {
	std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
	
	//Initialise DevIL image loader
	ilInit();

	//Set random seed for rand
	srand(time(NULL));

	// Init GLFW
	if (!glfwInit())
	{
		std::cout << "ERROR: Could not initialise GLFW...";
		return false;
	}

	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	// Create a GLFWwindow object that we can use for GLFW's functions
	window = glfwCreateWindow(WIDTH, HEIGHT, "Animation Project", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		std::cout << "ERROR: Could not create winodw...";
		return false;
	}

	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, mouse_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//Grab and hide mouse cursor
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	//glfwSetCursor(window, cursor);

	// Initialize GLEW
	glewExperimental = GL_TRUE; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return false;
	}

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS); 

	//Set up lighting in the scene
	Lighting::addLight(PointLight(glm::vec3(10.0f, 150.0f, 60.0f)));
	Lighting::addLight(DirectionalLight());

	skinningShader = ShaderManager::loadShader("skinning");
	texShader = ShaderManager::loadShader("texture");
	hand.load("../models/hand/hand.DAE");
	hand.setShader(skinningShader);
	hand.scale(glm::vec3(0.1f));

	bob.load("../models/bob/boblampclean.md5mesh");
	bob.setShader(skinningShader);
	//bob.rotateBy(glm::vec3(-1.57f, 0.0f, 0.0f));
	bob.translate(glm::vec3(40.0f, -25.0f, 0.0f));
	bob.scale(glm::vec3(1.0f));

	sphere.load("../models/sphere.obj");
	sphereTex = TextureUtils::LoadTextureFromFile("../textures/checkers.jpg");
	sphere.setShader(texShader);
	sphere.scale(glm::vec3(0.5f));
/*
	peter.load("../models/peter/peter.dae");
	peter.setShader(skinningShader);
	peter.rotateBy(glm::vec3(0.0f, -1.57f, 0.0f));
	peter.scale(glm::vec3(0.1f));
	peter.UpdateAnimation(0.0f);
*/	
	spaceStation.load("../models/space_station/space_station_4.obj");
	spaceStation.setShader(texShader);
	//spaceStation.scale(glm::vec3(2.0f));

	turtle.load("../models/turtle/turtle.dae");
	turtle.translate(glm::vec3(20.0f, 0.0f, 0.0f));
	turtle.setShader(skinningShader);
	//turtle.scale(glm::vec3(100.0f));
	//turtle.rotateBy(glm::vec3(-1.57f, 0.0f, 0.0f));

	zombie.LoadModel("../models/zombie/zombie_walk.dae");
	zombie.model.scale(glm::vec3(2.0f));
	zombie.model.setLocation(glm::vec3(-300,-25.0f, 200));
	zombie.model.setShader(skinningShader);
	zombie.model.animationStateMachine.allStates["idle"].isLooped = false;
	zombie.model.animationStateMachine.allStates["death"].isLooped = false;
	zombie.target = &stickMan.model.location;

	zombie1.LoadModel("../models/zombie/zombie_walk.dae");
	zombie1.model.scale(glm::vec3(2.0f));
	zombie1.model.setLocation(glm::vec3(450,-25.0f, -200));
	zombie1.model.setShader(skinningShader);
	zombie1.model.animationStateMachine.allStates["idle"].isLooped = false;
	zombie1.model.animationStateMachine.allStates["death"].isLooped = false;
	zombie1.target = &stickMan.model.location;

	zombie2.LoadModel("../models/zombie/zombie_walk.dae");
	zombie2.model.scale(glm::vec3(2.0f));
	zombie2.model.setLocation(glm::vec3(-500,-25.0f, 400));
	zombie2.model.setShader(skinningShader);
	zombie2.model.animationStateMachine.allStates["idle"].isLooped = false;
	zombie2.model.animationStateMachine.allStates["death"].isLooped = false;
	zombie2.target = &stickMan.model.location;

	zombie3.LoadModel("../models/zombie/zombie_walk.dae");
	zombie3.model.scale(glm::vec3(2.0f));
	zombie3.model.setLocation(glm::vec3(-100,-25.0f, -400));
	zombie3.model.setShader(skinningShader);
	zombie3.model.animationStateMachine.allStates["idle"].isLooped = false;
	zombie3.model.animationStateMachine.allStates["death"].isLooped = false;
	zombie3.target = &stickMan.model.location;

	zombie4.LoadModel("../models/zombie/zombie_walk.dae");
	zombie4.model.scale(glm::vec3(2.0f));
	zombie4.model.setLocation(glm::vec3(600,-25.0f, 300));
	zombie4.model.setShader(skinningShader);
	zombie4.model.animationStateMachine.allStates["idle"].isLooped = false;
	zombie4.model.animationStateMachine.allStates["death"].isLooped = false;
	zombie4.target = &stickMan.model.location;


	stickMan.LoadModel("../models/stick_man/stick_man_impatient.dae");
	stickMan.model.animationStateMachine.allStates["jump"].isLooped = false;
	stickMan.model.animationStateMachine.allStates["punch"].isLooped = false;
	stickMan.model.setShader(skinningShader);
	stickMan.model.setLocation(glm::vec3(30.0f, -25.0f, 0.0f));
	stickMan.model.scale(glm::vec3(0.75f));
	stickManTexture = TextureUtils::LoadTextureFromFile("../models/stick_man/Stick_Figure_by_Swp_All_White.BMP");

	//cubemap initialisation
	cubeMap.loadCubeMap("../textures/cubemaps/space/");
	cubeMap.setShader(ShaderManager::loadShader("skybox")); //TODO - This could definitely be hard-coded...

	DebugOutput::Init();

	return true;
}

// Moves/alters the camera positions based on user input
void handleInput()
{
/*
	if(keys[GLFW_KEY_LEFT_SHIFT])
	{
		if(keys[GLFW_KEY_D])
		{
			debug = !debug;
			keys[GLFW_KEY_D] = false;
		}
	}
*/
	// Camera controls
	if(keys[GLFW_KEY_Q])
	{
		camera.Position -= glm::vec3(0.0f, camera.MovementSpeed*deltaTime, 0.0f);//ProcessKeyboard(FORWARD, deltaTime);
	}

	if(keys[GLFW_KEY_P])
	{
		paused = !paused;
		keys[GLFW_KEY_P] = false;
	}

	if(keys[GLFW_KEY_E])
	{
		float punchRange = 120.0f;
		float zombieAngle;
		float dotZombie;

		if (stickMan.model.animationStateMachine.currentState->name != "punch")
		{
			stickMan.Punch();
	
			float minHitAngle = 1.57f;
			float maxHitAngle = 4.71f;

			dotZombie = glm::dot (glm::vec3(stickMan.model.modelMatrix[2]), glm::vec3(zombie.model.modelMatrix[2]));
			if (dotZombie > 1) dotZombie = 1;
			if (dotZombie < 0) dotZombie = 0;
			zombieAngle = glm::acos(dotZombie);		

			if (glm::length(zombie.model.getLocation() - stickMan.model.getLocation()) < punchRange && (zombieAngle > minHitAngle && zombieAngle < maxHitAngle) )
				zombie.Kill(stickMan.model.animationStateMachine.allStates["punch"].clip->mDuration);

			dotZombie = glm::dot (glm::vec3(stickMan.model.modelMatrix[2]), glm::vec3(zombie1.model.modelMatrix[2]));
			if (dotZombie > 1) dotZombie = 1;
			if (dotZombie < 0) dotZombie = 0;
			zombieAngle = glm::acos(dotZombie);	

			if (glm::length(zombie1.model.getLocation() - stickMan.model.getLocation()) < punchRange && (zombieAngle > minHitAngle && zombieAngle < maxHitAngle) )
				zombie1.Kill(stickMan.model.animationStateMachine.allStates["punch"].clip->mDuration);

			dotZombie = glm::dot (glm::vec3(stickMan.model.modelMatrix[2]), glm::vec3(zombie2.model.modelMatrix[2]));
			if (dotZombie > 1) dotZombie = 1;
			if (dotZombie < 0) dotZombie = 0;
			zombieAngle = glm::acos(dotZombie);	

			if (glm::length(zombie2.model.getLocation() - stickMan.model.getLocation()) < punchRange && (zombieAngle > minHitAngle && zombieAngle < maxHitAngle) )
				zombie2.Kill(stickMan.model.animationStateMachine.allStates["punch"].clip->mDuration);		

			dotZombie = glm::dot (glm::vec3(stickMan.model.modelMatrix[2]), glm::vec3(zombie3.model.modelMatrix[2]));
			if (dotZombie > 1) dotZombie = 1;
			if (dotZombie < 0) dotZombie = 0;
			zombieAngle = glm::acos(dotZombie);	

			if (glm::length(zombie3.model.getLocation() - stickMan.model.getLocation()) < punchRange && (zombieAngle > minHitAngle && zombieAngle < maxHitAngle) )
				zombie3.Kill(stickMan.model.animationStateMachine.allStates["punch"].clip->mDuration);

			dotZombie = glm::dot (glm::vec3(stickMan.model.modelMatrix[2]), glm::vec3(zombie4.model.modelMatrix[2]));
			if (dotZombie > 1) dotZombie = 1;
			if (dotZombie < 0) dotZombie = 0;
			zombieAngle = glm::acos(dotZombie);	

			if (glm::length(zombie4.model.getLocation() - stickMan.model.getLocation()) < punchRange && (zombieAngle > minHitAngle && zombieAngle < maxHitAngle) )
				zombie4.Kill(stickMan.model.animationStateMachine.allStates["punch"].clip->mDuration);
		}

		keys[GLFW_KEY_E] = false;
	}

	
	if(keys[GLFW_KEY_SPACE])
	{
		stickMan.Jump();
		keys[GLFW_KEY_SPACE] = false;
	}
	
	float targetMoveSpeed = 1.0f;
	if(keys[GLFW_KEY_LEFT_SHIFT] || keys[GLFW_KEY_RIGHT_SHIFT])
	{
		if(keys[GLFW_KEY_UP])
		{
			Lighting::pointLights[0].position -= glm::vec3(0.0f, 0.0f, targetMoveSpeed);
			//ikTarget -= glm::vec3(0.0f, 0.0f, targetMoveSpeed);
		}
		if(keys[GLFW_KEY_DOWN])
		{
			Lighting::pointLights[0].position += glm::vec3(0.0f, 0.0f, targetMoveSpeed);
			//ikTarget += glm::vec3(0.0f, 0.0f, targetMoveSpeed);
		}

		if (keys[GLFW_KEY_W])
		{
			stickMan.Run(deltaTime);
		}
	}
	else
	{
		if(keys[GLFW_KEY_W])
		{
			//camera.ProcessKeyboard(FORWARD, deltaTime);
			stickMan.Walk(deltaTime);
		}
		if(keys[GLFW_KEY_UP])
		{
			Lighting::pointLights[0].position += glm::vec3(0.0f, targetMoveSpeed, 0.0f);
			//ikTarget += glm::vec3(0.0f, targetMoveSpeed, 0.0f);
		}
		if(keys[GLFW_KEY_DOWN])
		{
			Lighting::pointLights[0].position -= glm::vec3(0.0f, targetMoveSpeed, 0.0f);
			//ikTarget -= glm::vec3(0.0f, targetMoveSpeed, 0.0f);
		}
		if(keys[GLFW_KEY_S])
		{
			camera.ProcessKeyboard(BACKWARD, deltaTime);
		}
	}

	if (!keys[GLFW_KEY_W])
	{
		stickMan.Idle();
	}

	if(keys[GLFW_KEY_A])
	{
		//camera.ProcessKeyboard(LEFT, deltaTime);
		stickMan.Turn(deltaTime);
	}
	if(keys[GLFW_KEY_D])
	{
		//camera.ProcessKeyboard(RIGHT, deltaTime);
		stickMan.Turn(-deltaTime);
	}
	if(keys[GLFW_KEY_LEFT])
	{
		Lighting::pointLights[0].position -= glm::vec3(targetMoveSpeed, 0.0f, 0.0f);
		//ikTarget -= glm::vec3(targetMoveSpeed, 0.0f, 0.0f);
	}
	if(keys[GLFW_KEY_RIGHT])
	{
		Lighting::pointLights[0].position += glm::vec3(targetMoveSpeed, 0.0f, 0.0f);
		//ikTarget += glm::vec3(targetMoveSpeed, 0.0f, 0.0f);
	}
}

//Is called when a mouse button is pushed
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT){
		switch (action)
		{
		case GLFW_RELEASE:
			break;
		case GLFW_PRESS:
			break;
		default:
			//what other actions are there for a mouse button?
			break;
		}
	}	 
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	//Had to put in this guard because media keys would crash the application
	if (key > 1024 || key < 0)
		return;

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

	if(action == GLFW_PRESS)
		keys[key] = true;
	else if(action == GLFW_RELEASE)
		keys[key] = false;
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	if(firstMouse)
	{
		lastX = (float) xpos;
		lastY = (float) ypos;
		firstMouse = false;
	}

	GLfloat xoffset = (float) xpos - lastX;
	GLfloat yoffset = lastY - ypos;  // Reversed since y-coordinates go from bottom to left
	
	lastX = xpos;
	lastY = ypos;

	glm::vec3 pitchAxis = glm::vec3(-stickMan.model.modelMatrix[0]);

	if (xoffset > 0.0001f || xoffset < -0.0001f)
		cameraOrientation *= glm::angleAxis(xoffset * deltaTime * 0.1f, glm::vec3(0,1,0));
	if (yoffset > 0.0001f || yoffset < -0.0001f)
		cameraOrientation *= glm::angleAxis(yoffset * deltaTime * 0.1f, pitchAxis);

	//camera.ProcessMouseMovement(xoffset, yoffset);
}	


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset * 0.1);
}