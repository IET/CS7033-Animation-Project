#pragma once
#include "character.h"
class Zombie :
	public Character
{
public:
	Zombie(void);

	float deathTime;
	
	float inverseSpeed;
	float stepSize;
	float turnSpeed;

	bool doDraw;

	glm::vec3 *target;

	void Update (float deltatime);
	void Draw(glm::mat4 v,glm::mat4  p, float deltaTime);

	//How can you kill that which has no life?
	//The below function, that's how.
	void Kill(float time);

	static std::vector<Zombie> allZombies;

	~Zombie(void);
};

