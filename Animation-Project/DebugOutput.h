#include <GL/glew.h>
#include <glm/glm.hpp>
#include "ShaderManager.hpp"
#include <vector>

#define MAX_LINES 50

struct DebugSphere{
	GLuint VBO, VAO;
	GLfloat sphereVertices[72];
};

struct DebugLine{
	GLuint VBO, VAO;
	GLfloat vertices[MAX_LINES * 3];
};


static class DebugOutput {
public:
	static DebugSphere	debugSphere;
	static DebugLine	debugLine;
	
	static void Init(glm::mat4 v = glm::mat4(1), glm::mat4 p = glm::mat4(1));
	
	//TODO - Implement something like this
	static void DrawLines(glm::mat4 modelMatrix, std::vector<glm::vec3> &startPoints, std::vector<glm::vec3> &endPoints, glm::mat4 v = DebugOutput::view, glm::mat4 p = DebugOutput::proj);

	//Add objects to the drawing queue
	static void DrawSphere(glm::vec3 pos, float radius, glm::mat4 m = glm::mat4(1), glm::vec3 col = glm::vec3(1.0f, 0.0f, 0.0f));
	static void DrawLine(glm::vec3 start, glm::vec3 end, glm::mat4 m= glm::mat4(1), glm::vec3 col = glm::vec3(0.0f, 0.0f, 1.0f));

	//Actually draw all of the geometry
	static void DrawAll(glm::mat4 v, glm::mat4 p);

	static Shader* shader;

	static void Update(glm::mat4 v, glm::mat4 p);

private:
	//TODO - Be more memory efficient
	struct Shape{
		glm::vec3 col;
		glm::mat4 m;
	};

	struct Sphere : Shape{
		glm::vec3 pos;
		float radius;
		
		Sphere(glm::vec3 pos, float radius, glm::vec3 col, glm::mat4 m)
			:pos(pos), radius(radius)
		{
			this->m = m;
			this->col = col;
		}
	};

	struct Line : Shape
	{
		glm::vec3 start;
		glm::vec3 end;
		glm::vec3 width;

		Line(glm::vec3 startPoint, glm::vec3 endPoint, glm::vec3 col, glm::mat4 m)
			:start(startPoint), end(endPoint)
		{
			this->m = m;
			this->col = col;
		}
	};

	static void DrawSphere(Sphere s, glm::mat4 v = DebugOutput::view, glm::mat4 p = DebugOutput::proj);
	static void DrawLine(Line l, glm::mat4 v = DebugOutput::view, glm::mat4 p = DebugOutput::proj);

	static glm::mat4 view;
	static glm::mat4 proj;
	static std::vector<Sphere> spheres;
	static std::vector<Line> lines;

};