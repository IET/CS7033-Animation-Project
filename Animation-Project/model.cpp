#include "model.h"
#include <assimp/matrix3x3.h>
#include "TextureUtils.h"
#include "DebugOutput.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/matrix_interpolation.hpp>
#include <regex>

#include <iostream>
#include <fstream>

/*  Functions   */
//Default constructor
Model::Model() 
{
	location = glm::vec3(0, 0, 0);
	quaternionOrientaton = glm::quat();
	numVerts = 0;
	numBones = 0;
	rootBone = nullptr;
	//path = "";

	this->modelMatrix = glm::mat4();
	ilInit();
}

// Constructor, expects a filepath to a 3D model.
Model::Model(string const & path, bool gamma) : gammaCorrection(gamma)
{
	location = glm::vec3(0, 0, 0);
	quaternionOrientaton = glm::quat();
	numVerts = 0;
	numBones = 0;
	rootBone = nullptr;
	//this->path = path;

	this->modelMatrix = glm::mat4();
	this->loadModel(path);
	ilInit();
}

//TODO - Get copy constructor and = operator to work correctly
// Copy constructor
Model::Model(const Model& other)
	:location(other.location), quaternionOrientaton(other.quaternionOrientaton), numVerts(other.numVerts), numBones(other.numBones)
{
	this->location = other.location;
	this->quaternionOrientaton = glm::quat();
}

Model Model::operator=(const Model &other )
{ 
	if (this == &other)
		return *this;

	this->location = other.location;
	this->quaternionOrientaton = glm::quat();

	return *this;
}

// Draws the model, and thus all its meshes
void Model::Draw(glm::mat4 &v, glm::mat4 &p)
{
	this->shader->enableShader();
	if (this->shader == NULL)
	{
		//Spam console output...
		cout << "Cannot draw model without shader";
		return;
	}

	this->shader->setUniformMatrix4fv("modelMat", this->modelMatrix);
	this->shader->setUniformMatrix4fv("viewMat", v);
	this->shader->setUniformMatrix4fv("projectionMat", p);
	for(GLuint i = 0; i < this->meshes.size(); i++)
		this->meshes[i].Draw(this->shader);

	this->shader->disableShader();
}

// Draws the model, and applies animation based on time input
void Model::Draw(glm::mat4 &v, glm::mat4 &p, float time)
{
	//std::vector<glm::mat4> transforms;
	//this->BoneTransform(time, transforms);
	this->Update(time);

	for (int i = 0 ; i < allBones.size() ; i++) 
	{
		std::ostringstream uniform;
		uniform << "gBones[" << i << "]";
		
		this->shader->setUniformMatrix4fv(uniform.str(), allBones[i].finalTransformation);
	}

	this->Draw(v, p);
}

void Model::UpdateAnimation(float time)
{
	this->shader->enableShader();
	std::vector<glm::mat4> transforms;
	this->BoneTransform(time, transforms);

	for (int i = 0 ; i < transforms.size() ; i++) 
	{
		std::ostringstream uniform;
		uniform << "gBones[" << i << "]";
		
		this->shader->setUniformMatrix4fv(uniform.str(), allBones[i].finalTransformation);
	}
}

// Draws the model, and applies animation based on time input
void Model::DrawIK(glm::mat4 &v, glm::mat4 &p, std::string ikRootBoneName, int ikLinkLength, glm::vec3 target)
{

	ikCCD(ikRootBoneName, ikLinkLength, target, 3);
	this->shader->enableShader();
	
	for (int i = 0 ; i < allBones.size() ; i++) 
	{
		std::ostringstream uniform;
		uniform << "gBones[" << i << "]";
		
		this->shader->setUniformMatrix4fv(uniform.str(), allBones[i].finalTransformation);
	}
	
	Draw(v, p);
}

void Model::DrawJoints()
{
	for (int i =0; i < allBones.size(); i ++)
		DebugOutput::DrawSphere(allBones[i].GetPosition(), 1.0f, this->modelMatrix, glm::vec3(0.0f, 1.0f, 0.0f)); 
}

void Model::DrawBones(glm::mat4 &v, glm::mat4 &p)
{
	DrawBones(v, p, rootBone);
}

void Model::DrawBones(glm::mat4 &v, glm::mat4 &p, Bone* bone)
{
	for (int i = 0; i < bone->children.size(); i++)
	{
		DebugOutput::DrawLine(glm::vec3(bone->GetPosition()), glm::vec3(bone->children[i]->GetPosition()), this->modelMatrix);
		DrawBones(v, p, bone->children[i]);
	}
}

Shader* Model::getShader() {
	return this->shader;
}

glm::mat4 Model::getModelMatrix() {
	return this->modelMatrix;
}

glm::vec3 Model::getLocation()
{
	return this->location;
}

void Model::setShader(Shader* s) {
	this->shader = s;
}

void Model::setLocation (glm::vec3 newPos) {
	location = newPos;

	modelMatrix[3] = glm::vec4(newPos, 1.0f);
}

void Model::moveBy(glm::vec3 move) {
	location.x += move.x;
	location.y += move.y;
	location.z += move.z;

	modelMatrix[3] = glm::vec4(location, 1.0f);
}

void Model::setRotation(glm::vec3 newRot) 
{
	this->modelMatrix = glm::mat4(1);
	quaternionOrientaton = glm::angleAxis(glm::length(newRot), glm::normalize(newRot));
	this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(newRot), glm::normalize(newRot)));
}
	
void Model::RotateBy(glm::vec3 rot) 
{
	//If there's zero rotation, get outta here
	if (glm::length(rot) < 0.0001f)
		return;

	quaternionOrientaton *= glm::angleAxis(glm::length(rot), glm::normalize(rot));
	//glm::mat4 rotMat = glm::mat4_cast(quaternionOrientaton);
	this->modelMatrix *= glm::mat4_cast(glm::angleAxis(glm::length(rot), glm::normalize(rot)));

	//Ensure location doesn't change (means I don't have to worry about order of operations)
	this->modelMatrix[3] = glm::vec4(location.x, location.y, location.z, 1.0f);
}

void Model::RotateBy(float angle, glm::vec3 axis) 
{
	//If there's zero rotation, get outta here
	if (angle < 0.0001f && angle > -0.0001f)
		return;

	quaternionOrientaton *= glm::angleAxis(angle, axis);
	//glm::mat4 rotMat = glm::mat4_cast(quaternionOrientaton);
	this->modelMatrix *= glm::mat4_cast(glm::angleAxis(angle, axis));

	//Ensure location doesn't change (means I don't have to worry about order of operations)
	this->modelMatrix[3] = glm::vec4(location.x, location.y, location.z, 1.0f);
}

void Model::translate(glm::vec3 t)
{
	this->modelMatrix = glm::translate(this->modelMatrix, t);
}

void Model::scale(glm::vec3 s)
{
	this->modelMatrix = glm::scale(this->modelMatrix, s);
}
	
void Model::load(string path)
{
	this->loadModel(path);
}

void Model::BoneTransform(const aiAnimation* animation, float time)
{
	if (animations.size() == 0)
	{
		//This is not an animated mesh
		return;
	}

	float ticksPerSecond = (float)(animation->mTicksPerSecond != 0 ? animation->mTicksPerSecond : 25.0f);
	float TimeInTicks = time * ticksPerSecond;
	//float AnimationTime = fmod(TimeInTicks, (float)animation->mDuration);

	ReadBoneHeirarchy(animation, TimeInTicks, rootBone, glm::mat4(1));

}

void Model::BoneTransform(float time, vector<glm::mat4> &transforms)
{
	transforms.resize(numBones);

	if (animations.size() == 0)
	{
		//This is not an animated mesh
		return;
	}

	float ticksPerSecond = (float)(animations[0]->mTicksPerSecond != 0 ? animations[0]->mTicksPerSecond : 25.0f);
	float TimeInTicks = time * ticksPerSecond;
	float AnimationTime = fmod(TimeInTicks, (float)animations[0]->mDuration);

	ReadBoneHeirarchy(animations[0], AnimationTime, rootBone, glm::mat4(1));
	
	for (int i = 0 ; i < numBones ; i++) {
		transforms[i] = allBones[i].finalTransformation;
	}
}

void Model::ReadBoneHeirarchy(const aiAnimation* pAnimation, float AnimationTime, Bone* bone, const glm::mat4 ParentTransform)
{
	if (animations.size() == 0)
	{
		std::cout << "Attempting to update animation on a model with no animations loaded.";
		return;
	}

	glm::mat4 NodeTransformation = glm::mat4(1);

	const aiNodeAnim* pNodeAnim = FindNodeAnim(pAnimation, bone->name);

	if (pNodeAnim) {
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling;
		CalcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
		bone->scale = glm::mat4(1);
		bone->scale[0][0] = Scaling.x;
		bone->scale[1][1] = Scaling.y;
		bone->scale[2][2] = Scaling.z;

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion RotationQ;
		CalcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);        
		bone->rotation = glm::mat4(1);
		aiMatrix3x3t<float> assimpRotationMatrix = RotationQ.GetMatrix();

		for (int i =0; i < 3; i++)
			for (int j =0; j < 3; j++)
				bone->rotation[i][j] = assimpRotationMatrix[j][i];

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation;
		CalcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
		bone->translation = glm::mat4(1);
		bone->translation[3][0] = Translation.x;
		bone->translation[3][1] = Translation.y;
		bone->translation[3][2] = Translation.z;

		// Combine the above transformations
		//NodeTransformation = TranslationM * RotationM * ScalingM;
		bone->Update();
	}
	bone->parentTransform = ParentTransform;
	bone->worldTransform = ParentTransform * bone->localTransform;
	
	
	if (boneMap.find(bone->name) != boneMap.end()) 
	{
		bone->finalTransformation = inverseTransform * bone->worldTransform * bone->boneOffset;
	}
	
	for (unsigned int i = 0 ; i < bone->children.size() ; i++) {
		ReadBoneHeirarchy(pAnimation, AnimationTime, bone->children[i], bone->worldTransform);
	}
} 

void Model::AnimationTransition(aiAnimation *a0, aiAnimation *a1, float fraction, float t0, float t1)
{
	AnimationTransition(a0, a1, fraction, rootBone, glm::mat4(1), t0, t1);
}

void Model::AnimationTransition(aiAnimation *a0, aiAnimation *a1, float fraction, Bone *bone, const glm::mat4 ParentTransform, float t0, float t1)
{
	glm::mat4 NodeTransformation = glm::mat4(1);

	const aiNodeAnim* pNodeAnim0 = FindNodeAnim(a0, bone->name);
	const aiNodeAnim* pNodeAnim1 = FindNodeAnim(a1, bone->name);


	if (pNodeAnim0 && pNodeAnim1) {
		// Interpolate scaling and generate scaling transformation matrix
		aiVector3D Scaling0;
		aiVector3D Scaling1;

		CalcInterpolatedScaling(Scaling0, t0, pNodeAnim0);
		CalcInterpolatedScaling(Scaling1, t1, pNodeAnim1);

		glm::vec3 s0, s1;

		s0.x = Scaling0.x; s0.y = Scaling0.y; s0.z = Scaling0.z;
		s1.x = Scaling0.x; s1.y = Scaling0.y; s1.z = Scaling0.z;

		//Linearly interpolate
		glm::vec3 interpolatedScale = s0 + fraction * (s1 - s0);
		
		bone->scale = glm::mat4(1);
		bone->scale[0][0] = interpolatedScale.x;
		bone->scale[1][1] = interpolatedScale.y;
		bone->scale[2][2] = interpolatedScale.z;

		// Interpolate rotation and generate rotation transformation matrix
		aiQuaternion Rotation0, Rotation1;

		CalcInterpolatedRotation(Rotation0, t0, pNodeAnim0);        
		CalcInterpolatedRotation(Rotation1, t1, pNodeAnim1);        

		aiQuaternion interpolatedRotation;
		aiQuaternion::Interpolate(interpolatedRotation, Rotation0, Rotation1, fraction);

		bone->rotation = glm::mat4(1);
		aiMatrix3x3t<float> assimpRotationMatrix = interpolatedRotation.GetMatrix();

		for (int i =0; i < 3; i++)
			for (int j =0; j < 3; j++)
				bone->rotation[i][j] = assimpRotationMatrix[j][i];

		// Interpolate translation and generate translation transformation matrix
		aiVector3D Translation0, Translation1;
		CalcInterpolatedPosition(Translation0, t0, pNodeAnim0);
		CalcInterpolatedPosition(Translation1, t1, pNodeAnim1);

		aiVector3D interpolatedTranslation = Translation0 + fraction * (Translation1 - Translation0);

		bone->translation = glm::mat4(1);
		bone->translation[3][0] = interpolatedTranslation.x;
		bone->translation[3][1] = interpolatedTranslation.y;
		bone->translation[3][2] = interpolatedTranslation.z;

		// Combine the above transformations
		//NodeTransformation = TranslationM * RotationM * ScalingM;
		bone->Update();
	}

	bone->parentTransform = ParentTransform;
	bone->worldTransform = ParentTransform * bone->localTransform;
	
	
	if (boneMap.find(bone->name) != boneMap.end()) 
	{
		bone->finalTransformation = inverseTransform * bone->worldTransform * bone->boneOffset;
	}
	
	for (unsigned int i = 0 ; i < bone->children.size() ; i++) {
		AnimationTransition(a0, a1, fraction, bone->children[i], bone->worldTransform,  t0, t1);
	}

}

void Model::Update(float deltaTime)
{
	animationStateMachine.Update(deltaTime);

	//If transitioning
	if (animationStateMachine.transitionEnd != -1 && animationStateMachine.nextState != nullptr)
	{
		float t = (animationStateMachine.currentTime - animationStateMachine.transitionStart) / (animationStateMachine.transitionEnd - animationStateMachine.transitionStart);

		float ticksPerSecond = (float)(animationStateMachine.currentState->clip->mTicksPerSecond != 0 ? animationStateMachine.currentState->clip->mTicksPerSecond : 25.0f);
		float t0 = animationStateMachine.transitionStart * ticksPerSecond;
		t0 = fmod(t0, (float)animationStateMachine.currentState->clip->mDuration);
		
		float t1;
		//For looped animation, seek to the same time in the target. This assumes all loops are of the same length
		if (animationStateMachine.nextState->isLooped)
		{
			ticksPerSecond = (float)(animationStateMachine.nextState->clip->mTicksPerSecond != 0 ? animationStateMachine.currentState->clip->mTicksPerSecond : 25.0f);
			t1 = animationStateMachine.transitionEnd * ticksPerSecond;
			t1 = fmod(t1, (float)animationStateMachine.nextState->clip->mDuration);
		}
		//For unlooped animations, simply play them from the start
		else
			t1 = 0;

		//Do the transitioning
		AnimationTransition(animationStateMachine.currentState->clip, animationStateMachine.nextState->clip, t, t0, t1);
	}
	//Not transitioning, just update bones based on the current state
	else
		BoneTransform(animationStateMachine.currentState->clip, animationStateMachine.currentTime);
}

void Model::ikCCD(const std::string &boneName, int linkLength, const glm::vec3 &target, int maxIterations)
{
	static const float IK_THRESHOLD = 1.1f;

	if (linkLength <= 0)
	{
		std::cout << "Error: IK chains must have positive non-zero length.";
		return;
	}

	std::vector<Bone*> targetBones;

	//TODO - Output an error if bone is not in map
	Bone* currentBone = &allBones[boneMap[boneName]];
	targetBones.push_back(currentBone);

	for (int i = 0; i < linkLength; i++)
	{
		if (! currentBone->children.size() > 0)
		{
			linkLength = i;
			break;
		}
		currentBone = currentBone->children[currentBone->children.size() -1];
		targetBones.push_back(currentBone);
	}

	glm::vec3 endEffectorPosition;
	if (targetBones[targetBones.size() -1]->children.size() > 0)
	{
		endEffectorPosition = targetBones[targetBones.size() -1]->children[0]->GetPosition();
	}
	else
	{
		endEffectorPosition = targetBones[targetBones.size() -1]->GetPosition();
	}
	//Convert End Effector to World Space
	endEffectorPosition = glm::vec3( modelMatrix * glm::vec4(endEffectorPosition, 1.0f));

	//Do Cyclic Coordinate Descent Calculations
	int iterations = 0;
	int currLink = targetBones.size() -1;

	float distance = glm::distance(endEffectorPosition, target);
	#ifdef _DEBUG
		DebugOutput::DrawSphere( endEffectorPosition, 1.5f);
	#endif
	
	while ( (distance = glm::distance(endEffectorPosition, target) > IK_THRESHOLD) 
			&& (iterations < maxIterations))
	{	
		glm::vec3 currentPos = glm::vec3 ( modelMatrix * glm::vec4(targetBones[currLink]->GetPosition(), 1.0f));

		#ifdef _DEBUG
			DebugOutput::DrawSphere( currentPos, 1.5f, glm::mat4(1), glm::vec3(0.0f, 0.0f, 1.0f));
		#endif

		glm::vec3 currentDir = glm::normalize(endEffectorPosition - currentPos);
		glm::vec3 targetDir	 = glm::normalize(target - currentPos);

		float cosAngle = glm::dot((currentDir), (targetDir));
		
		if (cosAngle > 1.0f || cosAngle < -1.0f)
		{
			if (cosAngle > 1.0f)
				cosAngle = 1.0f;

			if (cosAngle < -1.0f)
				cosAngle = -1.0f;
		}
		glm::vec3 rotAxis = (glm::cross(currentDir, targetDir));

		float angle = glm::acos(cosAngle);
		
		//Remember the importance of the w value. Having that as 1 was an error for so long!
		rotAxis = glm::vec3(  glm::inverse( modelMatrix * targetBones[currLink]->finalTransformation  * targetBones[currLink]->inverseBoneOffset) * glm::vec4(rotAxis, 0.0f));
		rotAxis = glm::normalize(rotAxis);
		
		glm::quat rotationQuaternion = glm::angleAxis(angle, rotAxis);
		glm::mat4 quaternionMatrix = glm::mat4_cast(rotationQuaternion);
		
		glm::vec3 eulerRotation = glm::eulerAngles(glm::quat(targetBones[currLink]->rotation));

		//TODO - Get Constraints working
/*
		//eulerRotation = glm::eulerAngles(rotationQuaternion);
		eulerRotation.x *= 180.0f;
		eulerRotation.x /= 3.14f;

		eulerRotation.y *= 180.0f;
		eulerRotation.y /= 3.14f;
		
		eulerRotation.z *= 180.0f;
		eulerRotation.z /= 3.14f;

		//Constrain motion
		if (eulerRotation.x > targetBones[currLink]->degreesOfFreedomMax.x)
			eulerRotation.x = targetBones[currLink]->degreesOfFreedomMax.x;
		if (eulerRotation.x < targetBones[currLink]->degreesOfFreedomMin.x)
			eulerRotation.x = targetBones[currLink]->degreesOfFreedomMin.x;

		if (eulerRotation.y > targetBones[currLink]->degreesOfFreedomMax.y)
			eulerRotation.y = targetBones[currLink]->degreesOfFreedomMax.y;
		if (eulerRotation.y < targetBones[currLink]->degreesOfFreedomMin.y)
			eulerRotation.y = targetBones[currLink]->degreesOfFreedomMin.y;

		if (eulerRotation.z > targetBones[currLink]->degreesOfFreedomMax.z)
			eulerRotation.z = targetBones[currLink]->degreesOfFreedomMax.z;
		if (eulerRotation.z < targetBones[currLink]->degreesOfFreedomMin.z)
			eulerRotation.z = targetBones[currLink]->degreesOfFreedomMin.z;

		eulerRotation.x /= 180.0f;
		eulerRotation.x *= 3.14f;

		eulerRotation.y /= 180.0f;
		eulerRotation.y *= 3.14f;
		
		eulerRotation.z /= 180.0f;
		eulerRotation.z *= 3.14f;
		
		targetBones[currLink]->rotation = glm::mat4_cast(glm::angleAxis(glm::length(eulerRotation), glm::normalize(eulerRotation)));

		//TODO - Test this
		rotationQuaternion = glm::quat(eulerRotation);
*/	
		//Update Curent Bone
		targetBones[currLink]->rotation *= quaternionMatrix;
		targetBones[currLink]->Update();
		targetBones[currLink]->worldTransform = targetBones[currLink]->parentTransform * targetBones[currLink]->localTransform;
		targetBones[currLink]->finalTransformation = inverseTransform * targetBones[currLink]->worldTransform * targetBones[currLink]->boneOffset;

		//Propagate Update to Child Bones 
		for (int i = 0; i < targetBones[currLink]->children.size(); i ++)
			UpdateBones(targetBones[currLink]->children[i], targetBones[currLink]->worldTransform);

		//Find New End Effector
		if (targetBones[targetBones.size() -1]->children.size() > 0)
		{
			endEffectorPosition = targetBones[targetBones.size() -1]->children[0]->GetPosition();
		}
		else
		{
			endEffectorPosition = targetBones[targetBones.size() -1]->GetPosition();
		}

		endEffectorPosition = glm::vec3( modelMatrix * glm::vec4(endEffectorPosition, 1.0f));

		currLink--;
		if (currLink < 0)
		{
			currLink = targetBones.size() -1;
			iterations++;
		}
	}

	//Make sure distance is up to date
	distance = glm::distance(endEffectorPosition, target);

#ifdef _DEBUG
	if (distance > IK_THRESHOLD)
		DebugOutput::DrawSphere( endEffectorPosition, 1.5f);
	else
		DebugOutput::DrawSphere( endEffectorPosition, 1.5f, glm::mat4(1), glm::vec3(0.0f, 1.0f, 0.0f));
#endif

}

//Given a root node, calculates the final transforms for bones all the way to the leaves
void Model::TransformBones(Bone* bone)
{
	for (int i = 0; i < bone->children.size(); i++)
	{
		bone->children[i]->finalTransformation = inverseTransform * bone->children[i]->worldTransform * bone->children[i]->boneOffset;
		TransformBones(bone->children[i]);
	}
}

void Model::LinearInterpolatePosition(glm::vec3 p0, glm::vec3 p1, float mu)
{
	this->setLocation(glm::vec3((p0 * (1 - mu)) + (p1 * mu)));
}

void Model::CosineInterpolatePosition(glm::vec3 p0, glm::vec3 p1, float mu)
{
	mu = (1 - cos(mu * 3.14))/2.0f;
	this->setLocation(glm::vec3((p0 * (1 - mu)) + (p1 * mu)));
}

void Model::CubicInterpolatePosition(glm::vec3 p0, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, float mu)
{
	mu = mu * mu;
	float a0, a1, a2, a3, a4;

	//a0 = (-0.5f * p0) + (1.5f * p1) - (1.5f * p2) + (0.5f * p3);
	//a1 = 
	this->setLocation(glm::vec3((p0 * (1 - mu)) + (p1 * mu)));
}

void Model::UpdateBones(Bone* bone, glm::mat4 parentTransform)
{
	//if (linkLength <= 0)
	//	return;
	//linkLength--;
	bone->parentTransform = parentTransform;
	bone->worldTransform = parentTransform * bone->localTransform;
	//bone->localTransform = glm::inverse(parentTransform) * bone->worldTransform;
	bone->finalTransformation = inverseTransform * bone->worldTransform * bone->boneOffset;

	for (int i = 0; i < bone->children.size(); i++)
	{
		UpdateBones(bone->children[i], bone->worldTransform);
	}

}


//TODO - This doesn't need to be in model Class...
glm::vec4 Model::findEndEffectorPosition(Bone *b)
{
	int numChildren = b->children.size();
	glm::vec4 summedPositions = glm::vec4(0);

	//This is a leaf node and so it should be close to the end effector
	//Return its value and average it with other leaf nodes below
	if (numChildren == 0)
	{
		return b->localTransform[3];
	}

	for (int i =0; i < b->children.size(); i++)
	{
		//TODO - If I'm doing it this way I'll have to update the child transforms as I go
		summedPositions += findEndEffectorPosition(b->children[i]);
	}

	return summedPositions/(float)numChildren;
}


const aiNodeAnim* Model::FindNodeAnim(const aiAnimation* pAnimation, const string NodeName)
{
	for (int i = 0 ; i < pAnimation->mNumChannels ; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];
		
		if (string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}
	
	return nullptr;
}
void Model::CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{

	if (pNodeAnim->mNumPositionKeys == 1) {
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}
	
	//Requires there to be a key at the start of the animation but avoids errors with erroneous factors
	//AnimationTime += pNodeAnim->mPositionKeys[0].mTime;

	unsigned int PositionIndex = FindPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	if (Factor < 0.0f) 
		Factor = 0.0f;
	if (Factor > 1.0f) 
		Factor = 1.0f;

	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void Model::CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	//Requires there to be a key at the start of the animation but avoids errors with erroneous factors
	//AnimationTime += pNodeAnim->mRotationKeys[0].mTime;

	unsigned int RotationIndex = FindRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	//Factor = fabs(Factor);
	
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	if (Factor < 0.0f) 
		Factor = 0.0f;
	if (Factor > 1.0f) 
		Factor = 1.0f;

	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ   = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;    
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}


void Model::CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	//Requires there to be a key at the start of the animation but avoids errors with erroneous factors
	//AnimationTime += pNodeAnim->mScalingKeys[0].mTime;

	unsigned int ScalingIndex = FindScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	//Factor = fabs(Factor);
	
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	if (Factor < 0.0f) 
		Factor = 0.0f;
	if (Factor > 1.0f) 
		Factor = 1.0f;

	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End   = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
} 
		
unsigned int Model::FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{

	for (int i = 0 ; i < pNodeAnim->mNumPositionKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime) {
			return i;
		}
	}

	//assert(0);
	return (float)pNodeAnim->mPositionKeys[pNodeAnim->mNumPositionKeys - 1].mTime;
}

unsigned int Model::FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (int i = 0 ; i < pNodeAnim->mNumRotationKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime) {
			return i;
		}
	}

//	assert(0);
	return (float)pNodeAnim->mPositionKeys[pNodeAnim->mNumRotationKeys - 1].mTime;
}

unsigned int Model::FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (int i = 0 ; i < pNodeAnim->mNumScalingKeys - 1 ; i++) {
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime) {
			return i;
		}
	}

//	assert(0);
	return (float)pNodeAnim->mPositionKeys[pNodeAnim->mNumScalingKeys - 1].mTime;
} 

Model::~Model() {
	for (int i = 0; i < animations.size(); i++)
	{
		if (animations[i] != nullptr)
			delete animations[i];
	}
}

//----------------------------------------------------------------------------------------------------------------
//-------------Private--------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------

/*  Functions   */
// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
void Model::loadModel(std::string path)
{
	minBounds = glm::vec3(100000000.0f, 100000000.0f, 100000000.0f);
	maxBounds = glm::vec3(-100000000.0f, -100000000.0f, -100000000.0f);
		
	// Read file via ASSIMP
	//When scene is initialised, it doesn't own the data, importer does so it can't go out of scope (unless I manage to make a deep copy)
	Assimp::Importer importer;

	//const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
	const aiScene* scene = importer.ReadFile(path, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);

	// Check for errors
	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) // if is Not Zero
	{
		std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
		return;
	}

	//Load root node inverse transform
	for (int i =0; i < 4; i++)
		for (int j =0; j < 4; j++)
			inverseTransform[i][j] = scene->mRootNode->mTransformation[i][j];
	inverseTransform = glm::inverse(inverseTransform);

	// Retrieve the directory path of the filepath
	this->directory = path.substr(0, path.find_last_of('/'));
	this->name = path.substr(directory.size() + 1, path.size());

	//Load animations
	LoadAnimations(scene);
	
	// Process ASSIMP's root node recursively
	this->processNode(scene->mRootNode, scene);
	
	//TODO - Build node hierarchy at this point. Bone map will be created so it'll just be another O(n) traversal to construct a tree of bones
	if (allBones.size() > 0)
	{
		BuildBoneHierarchy(scene->mRootNode);
		//Initialise bone transforms
		ReadBoneHeirarchy(animations[0], 0.0f, rootBone, glm::mat4(1));
	}
}

void Model::BuildBoneHierarchy(aiNode* node)
{
	string boneName(node->mName.data);

	//If this is a bone in the hierarchy (not all Assimp nodes are)
	if (boneMap.find(boneName) != boneMap.end() || boneName == "origin" || boneName == "Scene") 
	{
		//Special Case For Bob
		if (boneName == "origin" || boneName == "Scene")
		{
			Bone originBone;
			originBone.name = boneName;

			allBones.push_back(originBone);
			boneMap[originBone.name] = allBones.size() - 1;
		}

		if (rootBone == nullptr)	
		{
			rootBone = &(allBones[boneMap[boneName]]);
			//BuildHierarchy(node, rootBone);
		}

		for (int i = 0; i < node->mNumChildren; i++)
		{
			if (boneMap.find(node->mChildren[i]->mName.data) != boneMap.end()) 
				allBones[boneMap[boneName]].children.push_back(&(allBones[boneMap[node->mChildren[i]->mName.data]]));
		}
	}

	for (int i = 0; i < node->mNumChildren; i++)
	{
		//if (boneMap.find(node->mChildren[i]->mName.data) != boneMap.end()) 
		BuildBoneHierarchy(node->mChildren[i]);
	}
}

// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
void Model::processNode(aiNode* node, const aiScene* scene)
{
	// Process each mesh located at the current node
	for(GLuint i = 0; i < node->mNumMeshes; i++)
	{
		// The node object only contains indices to index the actual objects in the scene. 
		// The scene contains all the data, node is just to keep stuff organized (like relations between nodes).
		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]]; 
		this->meshes.push_back(this->processMesh(mesh, scene));			
	}
	// After we've processed all of the meshes (if any) we then recursively process each of the children nodes
	for(GLuint i = 0; i < node->mNumChildren; i++)
	{
		this->processNode(node->mChildren[i], scene);
	}
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
	// Data to fill
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	
	// Walk through each of the mesh's vertices
	for(GLuint i = 0; i < mesh->mNumVertices; i++)
	{
		Vertex vertex;
		glm::vec3 vector; // We declare a placeholder std::vector since assimp uses its own std::vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
		// Positions
		vector.x = mesh->mVertices[i].x;
		vector.y = mesh->mVertices[i].y;
		vector.z = mesh->mVertices[i].z;
		vertex.Position = vector;

		//Check bounds
		for (int j = 0; j < 3; j++) {
			if (vector[j] < minBounds[j])
				minBounds[j] = vector[j];
			if (vector[j] > maxBounds[j])
				maxBounds[j] = vector[j];
		}
		// Normals
		if (mesh->mNormals != nullptr)
		{
			vector.x = mesh->mNormals[i].x;
			vector.y = mesh->mNormals[i].y;
			vector.z = mesh->mNormals[i].z;
			vertex.Normal = vector;
		}
		else
		{
			vertex.Normal = glm::vec3(1);
		}

		// Texture Coordinates
		if(mesh->mTextureCoords[0]) // Does the mesh contain texture coordinates?
		{
			glm::vec2 vec;
			// A vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't 
			// use models where a vertex can have multiple texture coordinates so we always take the first set (0).
			vec.x = mesh->mTextureCoords[0][i].x; 
			vec.y = mesh->mTextureCoords[0][i].y;
			vertex.TexCoords = vec;
		}
		else
			vertex.TexCoords = glm::vec2(0.0f, 0.0f);
			
		if (mesh->mTangents != NULL) {
			// Tangent
			vector.x = mesh->mTangents[i].x;
			vector.y = mesh->mTangents[i].y;
			vector.z = mesh->mTangents[i].z;
			vertex.Tangent = vector;
			// Bitangent
			vector.x = mesh->mBitangents[i].x;
			vector.y = mesh->mBitangents[i].y;
			vector.z = mesh->mBitangents[i].z;
			vertex.Bitangent = vector;
		}
		else
		{
			vertex.Tangent = glm::vec3(0.0f, 0.0f, 0.0f);				
			vertex.Bitangent = glm::vec3(0.0f, 0.0f, 0.0f);
		}
		vertices.push_back(vertex);
	}

	//Load any bones present in mesh
	for (unsigned int i = 0 ; i < mesh->mNumBones ; i++) {                
		unsigned int BoneIndex = 0;        
		string BoneName(mesh->mBones[i]->mName.data);

		if (boneMap.find(BoneName) == boneMap.end()) {
			// Allocate an index for a new bone
			BoneIndex = allBones.size();
			numBones++;           
			Bone bi;			
			bi.name = BoneName;

			if (std::regex_match(bi.name, std::regex("[Uu]pperarm\.[RrLl]")))
			{
				bi.degreesOfFreedomMax = glm::vec3(180.0f);
				bi.degreesOfFreedomMin = glm::vec3(-180.0f);
			}

			if (std::regex_match(bi.name, std::regex("[Ff]orearm\.[RrLl]")))
			{
				bi.degreesOfFreedomMax = glm::vec3(180.0f);				
				bi.degreesOfFreedomMin = glm::vec3(-180.0f);
			}

			if (std::regex_match(bi.name, std::regex("[Ww]rist\.[RrLl]")))
			{
				bi.degreesOfFreedomMax = glm::vec3(180.0f);
				bi.degreesOfFreedomMin = glm::vec3(-180.0f);
			}

			allBones.push_back(bi);
			for (int j =0; j < 4; j++)
				for (int k =0; k < 4; k++)	
					allBones[allBones.size() -1 ].boneOffset[j][k] = mesh->mBones[i]->mOffsetMatrix[k][j];            
			
			allBones[allBones.size() -1 ].inverseBoneOffset = glm::inverse(allBones[allBones.size() -1 ].boneOffset);

			boneMap[BoneName] = BoneIndex;
		}
		else {
			BoneIndex = boneMap[BoneName];
		}                      
		
		for (unsigned int j = 0 ; j < mesh->mBones[i]->mNumWeights ; j++) {
			unsigned int VertexID = mesh->mBones[i]->mWeights[j].mVertexId;// - numVerts;
			float Weight  = mesh->mBones[i]->mWeights[j].mWeight;
			vertices[VertexID].AddBoneData(BoneIndex, Weight);
		}
	}  

	// Now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
	for(GLuint i = 0; i < mesh->mNumFaces; i++)
	{
		aiFace face = mesh->mFaces[i];
		// Retrieve all indices of the face and store them in the indices std::vector
		for(GLuint j = 0; j < face.mNumIndices; j++)
			indices.push_back(face.mIndices[j]);
	}
	// Process materials
	if(mesh->mMaterialIndex >= 0)
	{
		aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];
		// We assume a convention for sampler names in the shaders. Each diffuse texture should be named
		// as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER. 
		// Same applies to other texture as the following list summarizes:
		// Diffuse: texture_diffuseN
		// Specular: texture_specularN
		// Normal: texture_normalN

		// 1. Diffuse maps
		std::vector<Texture> diffuseMaps = this->loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
		textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
		// 2. Specular maps
		std::vector<Texture> specularMaps = this->loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
		textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
		// 3. Normal maps
		vector<Texture> normalMaps = this->loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
		textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
		// 4. Height maps
		vector<Texture> heightMaps = this->loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
		textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
	}
		
	numVerts += vertices.size();
	// Return a mesh object created from the extracted mesh data
	return Mesh(vertices, indices, textures);
}

void Model::LoadAnimations(const aiScene* scene)
{
	if (scene->mNumAnimations > 0)	
		animationStateMachine.AddState("idle", LoadAnimation(scene->mAnimations[0]));
	else
		return;

	std::ifstream in(directory + '/' + "AnimationController.anim");

	enum ParserState {ANIMATIONS, TRANSITIONS};
	int currParserState = 0;
	//If there's an animation controller file present
	if (in)
	{
		std::string line;
		while ( getline(in, line))
		{
			//Ignore any comments or blank lines in the file
			if (line[0] == '#' || line == "")
				continue;

			if (currParserState == ParserState::ANIMATIONS)
			{
				if (line == "T")
				{
					currParserState = ParserState::TRANSITIONS;
					continue;
				}

				std::string name = 	line.substr(0, line.find_last_of(':') - 1);
				std::string animationPath = directory + '/';
				animationPath += line.substr(line.find_last_of(':') + 2, line.size());

				Assimp::Importer importer;
				const aiScene* animationFile = importer.ReadFile(animationPath, aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
				
				// Check for errors
				if(!animationFile || animationFile->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !animationFile->mRootNode) // if is Not Zero
				{
					std::cout << "ERROR::ASSIMP:: " << importer.GetErrorString() << endl;
					continue;
				}

				if (animationFile->mNumAnimations < 1)
				{
					std::cout << "ERROR::No animations found in file: animationPath";
					continue;
				}
				
				aiAnimation* anim = animationFile->mAnimations[0];
				animationStateMachine.AddState(name, LoadAnimation(anim));
			}
			else if (currParserState == ParserState::TRANSITIONS)
			{
				std::string startState	= line.substr(0, line.find_last_of('>') - 1);
				int startEndStateParse = line.find_last_of('>') + 2;
				std::string endState	= line.substr(startEndStateParse, line.find_last_of(':') - startEndStateParse - 1);
				float time = atof(line.substr(line.find_last_of(':') + 2, line.size()).c_str());
				
				animationStateMachine.AddTransition(startState, endState, time);
			}
		}
	}
}

aiAnimation* Model::LoadAnimation(const aiAnimation* anim)
{
	aiAnimation* animation = new aiAnimation();
	animation->mChannels = new aiNodeAnim*[anim->mNumChannels];//anim->mChannels;
	animation->mDuration = anim->mDuration;
	animation->mMeshChannels = anim->mMeshChannels;
	animation->mName = anim->mName;
	animation->mNumChannels = anim->mNumChannels;
	animation->mNumMeshChannels = anim->mNumMeshChannels;
	animation->mTicksPerSecond = anim->mTicksPerSecond;
		
	for (int j =0; j < animation->mNumChannels; j++)
	{
		animation->mChannels[j] = new aiNodeAnim();
		animation->mChannels[j]->mNodeName			= anim->mChannels[j]->mNodeName;
		animation->mChannels[j]->mNumPositionKeys	= anim->mChannels[j]->mNumPositionKeys;
		animation->mChannels[j]->mNumRotationKeys	= anim->mChannels[j]->mNumRotationKeys;
		animation->mChannels[j]->mNumScalingKeys	= anim->mChannels[j]->mNumScalingKeys;

		animation->mChannels[j]->mPositionKeys = new aiVectorKey[animation->mChannels[j]->mNumPositionKeys];
		for (int k = 0; k < animation->mChannels[j]->mNumPositionKeys; k++)
			animation->mChannels[j]->mPositionKeys[k] = anim->mChannels[j]->mPositionKeys[k];
		
		animation->mChannels[j]->mRotationKeys = new aiQuatKey[animation->mChannels[j]->mNumRotationKeys];
		for (int k = 0; k < animation->mChannels[j]->mNumRotationKeys; k++)
			animation->mChannels[j]->mRotationKeys[k] = anim->mChannels[j]->mRotationKeys[k];

		animation->mChannels[j]->mScalingKeys = new aiVectorKey[animation->mChannels[j]->mNumScalingKeys];
		for (int k = 0; k < animation->mChannels[j]->mNumScalingKeys; k++)
			animation->mChannels[j]->mScalingKeys[k] = anim->mChannels[j]->mScalingKeys[k];

		animation->mChannels[j]->mPostState = anim->mChannels[j]->mPostState;
		animation->mChannels[j]->mPreState = anim->mChannels[j]->mPreState;
	}

	animation->mMeshChannels = new aiMeshAnim*[animation->mNumMeshChannels];
	for (int j =0; j < animation->mNumMeshChannels; j++)
	{
		animation->mMeshChannels[j]->mName = anim->mMeshChannels[j]->mName;
		animation->mMeshChannels[j]->mNumKeys = anim->mMeshChannels[j]->mNumKeys;
		animation->mMeshChannels[j]->mKeys = new aiMeshKey[animation->mMeshChannels[j]->mNumKeys];

		for (int k = 0; k < animation->mMeshChannels[j]->mNumKeys; k++)
		{
			animation->mMeshChannels[j]->mKeys[k] = anim->mMeshChannels[j]->mKeys[k];
		}
	}

	animations.push_back(animation);
	return animation;
}


// Checks all material textures of a given type and loads the textures if they're not loaded yet.
// The required info is returned as a Texture struct.
std::vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName)
{
	std::vector<Texture> textures;
	for(GLuint i = 0; i < mat->GetTextureCount(type); i++)
	{
		aiString str;
		mat->GetTexture(type, i, &str);
		// Check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
		GLboolean skip = false;
		for(GLuint j = 0; j < textures_loaded.size(); j++)
		{
			if(textures_loaded[j].path == str)
			{
				textures.push_back(textures_loaded[j]);
				skip = true; // A texture with the same filepath has already been loaded, continue to next one. (optimization)
				break;
			}
		}
		if(!skip)
		{   // If texture hasn't been loaded already, load it
			Texture texture;
			texture.id = TextureUtils::LoadTextureFromFile((GLchar*)(this->directory + '/' + std::string(str.C_Str())).c_str());
			texture.type = typeName;
			texture.path = str;
			textures.push_back(texture);
			this->textures_loaded.push_back(texture);  // Store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
		}
	}
	return textures;
}

GLint Model::TextureFromFile(const char* path, std::string directory, bool gamma)
{
	//Generate texture ID and load texture data 
	std::string filename = std::string(path);
	filename = directory + '/' + filename;
	GLuint textureID;
	glGenTextures(1, &textureID);
	int width,height;

	ILuint imageID;
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilEnable(IL_ORIGIN_SET);
	ilOriginFunc(IL_ORIGIN_UPPER_LEFT/*IL_ORIGIN_LOWER_LEFT*/);

	if (ilLoadImage((ILstring)path)) {
		// Assign texture to ID
		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, gamma ? GL_SRGB : GL_RGBA, ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, GL_RGB, GL_UNSIGNED_BYTE, ilGetData());
		glGenerateMipmap(GL_TEXTURE_2D);	

		// Parameters
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		return textureID;
	} else {
		return -1;
	}
}