#pragma once
#include "AnimationState.h"
#include <string>
#include <vector>
#include <unordered_map>

class AnimationStateMachine
{
public:
	AnimationStateMachine(void);
	
	std::unordered_map<std::string, AnimationState> allStates;
	
	void Update(float deltaTime);

	void Play();
	void Pause();

	void AddState(std::string name, aiAnimation* clip);
	void AddTransition(std::string source,std::string destination, float time);

	bool ChangeState(std::string targetState);
	bool StateBlip(std::string targetState);

	~AnimationStateMachine(void);

public:
	float currentTime;
	bool isPlaying;

	float transitionStart, transitionEnd, blipTime;

	AnimationState* currentState;
	//Should be nullptr unless currently transitioning
	AnimationState* nextState, *previousState;
};

