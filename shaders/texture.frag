#version 330 core
out vec4 FragColor;

const int MAX_POINT_LIGHTS = 7;
const int MAX_SPOT_LIGHTS = 2;



in VS_OUT {
	vec3 WorldPos;
	vec3 Normal;
	vec2 TexCoords;
	vec3 TangentLightPos;
	vec3 TangentViewPos;
	vec3 TangentFragPos;
} fs_in;

struct Attenuation                                                                  
{                                                                                   
	float Constant;                                                                 
	float Linear;                                                                   
	float Exp;                                                                      
};       

struct BaseLight
{
	vec3 Color;
	float AmbientIntensity;
	float DiffuseIntensity;
};                                                                           
																					
struct PointLight                                                                           
{                                                                                           
	BaseLight Base;                                                                  
	vec3 Position;                                                                          
	Attenuation Atten;                                                                      
};                                                                                          
																							
struct SpotLight                                                                            
{                                                                                           
	PointLight Base;                                                                 
	vec3 Direction;                                                                         
	float Cutoff;                                                                           
};   

struct DirectionalLight
{
	BaseLight Base;
	vec3 Direction;
};

uniform sampler2D texture_diffuse1;

//TODO - The below two should be uniforms
float matSpecularIntensity = 0.1f;
float specularPower = 5.0f;

uniform vec3 camPos;
uniform int numPointLights;                                                                
uniform int numSpotLights;
																 
uniform DirectionalLight directionalLight;                                                 
uniform PointLight 		 pointLights[MAX_POINT_LIGHTS];                                          
uniform SpotLight 		 spotLights[MAX_SPOT_LIGHTS];                                             

vec4 CalcLightInternal(BaseLight Light, vec3 LightDirection)            
{                                                                                           
	vec4 AmbientColor = vec4(Light.Color * Light.AmbientIntensity, 1.0);
	float DiffuseFactor = dot(fs_in.Normal, LightDirection);                                     
																							
	vec4 DiffuseColor  = vec4(0, 0, 0, 0);                                                  
	vec4 SpecularColor = vec4(0, 0, 0, 0);                                                  
																							
	if (DiffuseFactor > 0.0) {                                         
		DiffuseColor = vec4(Light.Color * Light.DiffuseIntensity * DiffuseFactor, 1.0);
															
		vec3 VertexToEye = normalize(camPos - fs_in.WorldPos);              
		vec3 LightReflect = normalize(reflect(LightDirection, fs_in.Normal));
		float SpecularFactor = dot(VertexToEye, LightReflect);
		if (SpecularFactor > 0.0) {                                                         
			SpecularFactor = pow(SpecularFactor, specularPower);
			SpecularColor = vec4(Light.Color * matSpecularIntensity * SpecularFactor, 1.0);
		}                                                                                   
	}                                                                                       
																							
	return (AmbientColor + DiffuseColor + SpecularColor);                                   
}

vec4 CalcPointLight(PointLight l)                                       
{                                                                                           
	vec3 LightDirection = fs_in.WorldPos - l.Position;                                           
	float Distance = length(LightDirection);                                                
	LightDirection = normalize(LightDirection);                                             
																							
	vec4 Color = CalcLightInternal(l.Base, LightDirection);                         
	float Attenuation =  l.Atten.Constant +                                                 
						 l.Atten.Linear * Distance +                                        
						 l.Atten.Exp * Distance * Distance;                                 
																							
	return Color / Attenuation;                                                             
}

vec4 CalcDirectionalLight()                                                      
{                                                                                           
	return CalcLightInternal(directionalLight.Base, directionalLight.Direction);  
}   

void main()
{
	// Linearly interpolate between both textures (second texture is only slightly combined)

	vec4 totalLight = vec4(0,0,0,0);
	
	for (int i = 0; i < numPointLights; i++)
		totalLight += CalcPointLight(pointLights[i]);

	totalLight += CalcDirectionalLight();

	vec3 color = texture(texture_diffuse1, fs_in.TexCoords).rgb * totalLight.rgb;

	FragColor = vec4(color.rgb, 1.0f);
	//FragColor = vec4(totalLight);
	//FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}