# Final Project of the CS7033 Real Time Animation Module For the IET Masters

In this program you control a 3D stickman model in a space station overrun with zombies and you have no choice but to punch your way out of there.

I have implemented an animation state-machine that uses an external file to define different animations for a model where each line defines a new animation as

> \<animation_nane\> : \<animation_filepath\>

and transitions are defined as

> \<start_animation\>  \>  \<end_animation\> : \<transition_time\>

### Controls
WASD to move character, mouse to control camera

E to punch zombies

Space to jump

### Links
[YouTube Video](https://www.youtube.com/watch?v=f-kYUYmaf_g&feature=youtu.be)

[Binary](https://www.dropbox.com/sh/bq768aor61rbvsq/AAAMb7lUwIR3A6sQJuAxvv_Ka?dl=0)

## Models Used
[Stick Man](http://tf3dm.com/3d-model/rigged-stick-figure-by-swp-2-55987.html)

[Zombie](http://www.models-resource.com/gamecube/residentevil0/)

[Space Station](http://www.turbosquid.com/FullPreview/Index.cfm/ID/554462)