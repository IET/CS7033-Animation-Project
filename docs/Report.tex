\documentclass[a4paper]{report} % V1.2
\usepackage[]{minted} 	%Code highlighting
\usepackage{gensymb}	%Degree symbol
\usepackage{listings}	%Alternate code listings
\usepackage{float}
\usepackage{wrapfig}

%Adds a box around all your figures
%\floatstyle{boxed} 
%\restylefloat{figure}

\usepackage{graphicx}	%For includegraphics in figures

% \usepackage{tcolorbox}
% \usepackage{etoolbox}
% \BeforeBeginEnvironment{minted}{\begin{tcolorbox}}%
% \AfterEndEnvironment{minted}{\end{tcolorbox}}%
\lstset{language=c++, aboveskip=0pt}
\usepackage{etoolbox}

%\doi{10.1145/1559755.1559763}

% Copyright
%\setcopyright{acmcopyright}

\begin{document}

% Page heads

% Title portion
\title{CS7033\\ Real-Time Animation\\ Final Project}


\author{
	%\alignauthor
		Daniel Walsh\\
	 	{Trinity College Dublin}\\
		{walshd15@tcd.ie}
}

\maketitle



\section{Introduction}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{zombie_chase}
	\caption{Screenshot of Application With Stickman in His Idle State With Many Zombies Pursuing }
	\label{fig:zombie_chase}
\end{figure}

In this program, you play as a stick figure on a space station being pursued by zombies and you must punch all of the zombies to defeat them. Both the stickman and the zombies are rigged models using linear blend skinning, and I have implemented a state machine to switch between different animations for each model.

\section{Technical Detail}

\subsection{Animation State Machine}
In order to support switching between different animations for each model I implemented a state machine that is constructed based on a file named ``AnimationController.anim", if this file is found in the same directory as the idle animation for this character, the importer will construct a state machine based on the contents of this file. Listing~\ref{listing:sm_stickman} shows the entire state machine definition for the Stickman model. The original file that the importer is directed towards is assigned as the ``idle'' pose and the animation controller file begins with a  listing of states in the following format $state\_name : relative\_path\_to\_file$. The letter T is then used to change the state of the importer and it begins loading state transitions that are defined in the format $start\_state > end\_state : transition\_time$. The timings for the state transitions are used to define a short blend period where the two animations are interpolated to allow for a smoother transition.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{python}
#Files
walk : animations/stick_man_walk.dae
run : animations/stick_man_run.dae
jump : animations/stick_man_jump2.dae
punch : animations/stick_man_punch.dae

#State Transitions
T
idle > walk : 0.5
idle > run : 0.1
idle > jump : 1
idle > punch : 0.2

walk > idle : 1
walk > run : 0.5
walk > jump : 0.5
walk > punch : 0.2

run > idle : 1
run > walk : 0.5
run > jump : 0.5
run > punch : 0.1

jump > walk : 0.5
jump > run : 0.5
jump > idle : 1

punch > idle : 1
punch > walk : 0.5
punch > run : 0.2
\end{minted}
\caption{The Stickman Model's Animation Controller File.}
\label{listing:sm_stickman}
\end{listing}

Each model in this program has a state machine (though if there is no animation controller present, it will only have one state). Updating the model's bones based on the state machine is handled in the model's update method, shown in Listing~\ref{listing:model_update}. Each character also has their own class that is used to define actions that cause transitions in the state machine. 

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize, breaklines=true]{c++}
void Model::Update(float deltaTime)
{
	animationStateMachine.Update(deltaTime);
	//If transitioning
	if (animationStateMachine.transitionEnd != -1 && animationStateMachine.nextState != nullptr)
	{
		float t = (animationStateMachine.currentTime - animationStateMachine.transitionStart) / (animationStateMachine.transitionEnd - animationStateMachine.transitionStart);

		float ticksPerSecond = (float)(animationStateMachine.currentState->clip->mTicksPerSecond != 0 ? animationStateMachine.currentState->clip->mTicksPerSecond : 25.0f);

		float t0 = animationStateMachine.transitionStart * ticksPerSecond;
		t0 = fmod(t0, (float)animationStateMachine.currentState->clip->mDuration);
		
		float t1;
		//For looped animation, seek to the same time in the target. This assumes all loops are of the same length
		if (animationStateMachine.nextState->isLooped)
		{
			ticksPerSecond = (float)(animationStateMachine.nextState->clip->mTicksPerSecond != 0 ? animationStateMachine.currentState->clip->mTicksPerSecond : 25.0f);

			t1 = animationStateMachine.transitionEnd * ticksPerSecond;
			t1 = fmod(t1, (float)animationStateMachine.nextState->clip->mDuration);
		}
		//For unlooped animations, simply play them from the start
		else
			t1 = 0;

		//Do the transitioning
		AnimationTransition(animationStateMachine.currentState->clip, animationStateMachine.nextState->clip, t, t0, t1);
	}
	//Not transitioning, just update bones based on the current state
	else
		BoneTransform(animationStateMachine.currentState->clip, animationStateMachine.currentTime);
}
\end{minted}
\caption{Model Update Based on State Machine.}
\label{listing:model_update}
\end{listing}

The interpolation between animations is performed with a linear interpolation for the translation and scale, while rotation is handled by Assimp's $aiQuaternion::Interpolate$ method.
All of the state transitions are based on timing so for a standard transition, the $transitionTime$ variable will be set and the state will actually be set at this time. Similarly, for a state blip, the variable $blipTime$ will be set and the state machine will return to its previous state at this time. Listing~\ref{listing:state_machine_update} shows the state machine's update method, including how these timings are managed.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize, breaklines=true]{c++}
void AnimationStateMachine::Update(float deltaTime)
{

	if (!isPlaying)
		return;

	currentTime += deltaTime;

	//If at the end of a transition, switch to new state
	if (transitionEnd != -1 && currentTime >= transitionEnd)
	{
		currentState = nextState;
		nextState = nullptr;
		transitionEnd = -1;

		if (currentTime > currentState->clip->mDuration)
			currentTime = fmod(currentTime, (float)currentState->clip->mDuration);

		if (!currentState->isLooped)
			currentTime = 0;
	}

	//Return to previous state at the end of a blip	
	if (blipTime != -1 && currentTime >= blipTime && transitionEnd == -1)
	{
		ChangeState(previousState->name);
		blipTime = -1;
	}

	//Reset a looped animation
	if (currentTime > currentState->clip->mDuration && transitionEnd == -1)
	{
		if (currentState->isLooped)
			currentTime = 0;
	}
}
\end{minted}
\caption{State Machine Update.}
\label{listing:state_machine_update}
\end{listing}



\section {Principles of Animation}

\subsection{Anticipation and Timing}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{punch_timing2}
	\caption{Timeline of Stickman Punch Animation Showing Keyframe Spacing}
	\label{fig:punch_timing}
\end{figure}

In the Stickman punch animation, there are ten frames of anticipation, followed by five frames of action. The final punch position is then held for three frames to emphasise the action before taking twelve frames to return to the base pose.


\subsection{Follow Through}

\begin{figure}[H]
\minipage {0.45\textwidth}
\includegraphics[width=\linewidth]{Stickman_jump_l_foot}
\endminipage \hfill
\minipage {0.45\textwidth}
\includegraphics[width=\linewidth]{Stickman_jump_final_frame}
\endminipage \hfill
\caption{Two Frames of the Stickman's Jump Animation Showing Follow-Through}
\label{figure:Stickman_jump}
\end{figure}

In The Stickman's jump animatiion, his left foot touches down first and there are another ten frames before the right leg and right arm have returned to the base pose.

\subsection{Straight Ahead vs. Pose-to-Pose}

All of the animations in this project are keyframe animations built in Maya producing pose-to-pose style animations.

\subsection{Arcs}

%\begin{wrapfigure}{R}{0.35\textwidth}
%	\includegraphics[width=0.25\textwidth]{walk_arc}
%	\label{fig:walk_arc}
%\end{wrapfigure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{walk_arc}
	\caption{Motion Along Arc of Arms and Legs in Stickman Walk Animation}
	\label{fig:walk_arc}
\end{figure}

In the walk and run animations of the Stickman character, the arms and legs carve out a natural arc in their motion back and forth, as shown in Figure~\ref{fig:walk_arc}. Many other animations in the project have this property, such as the forward path of the right hand in the punch animation and the backward trajectory of the zombie in its death animation.

\subsection{Exaggeration}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.75\textwidth]{punch_exaggeration}
	\caption{Anticipation and Action Poses of Punching Animation Showing Translation of Entire Model}
	\label{fig:punch_exaggeration}
\end{figure}

The Stickman punch animation is highly exaggerated with the entire model shifting forward at the moment of impact, as shown in Figure~\ref{fig:punch_exaggeration}. The zombie death animation is also highly exaggerated with the zombies tumbling backwards as they are defeated.

\subsection{Appeal}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\textwidth]{Stickman_sass}
	\caption{Stickman Becoming Impatient With the Lack of Input.}
	\label{fig:Stickman_sass}
\end{figure}

The Stickman's idle animation has him standing with arms crossed and after remaining idle for long enough, he will start tapping his foot and look at his watch, as shown in Figure~\ref{fig:Stickman_sass}, this gives a better sense of personality to the character than if it was simply a static idle animation or a constant walk loop.

There is also a general lack of symmetry in the animations produced. The most obvious example is the zombies dragging one of their legs and reaching with one arm while the other hangs loosely meaning that they are almost completely asymmetric. There is also a random offset applied to their movement speeds and start times so that they are not all moving in sync.


% Bibliography
%\bibliographystyle{abbrv}
%\bibliography{references}
\end{document}
